package com.ccanabalr.piggybank.application.helper;

import java.util.List;
import com.ccanabalr.piggybank.domain.PiggyBank;

public interface PiggyBankHelper {
  public Integer amountCoins(List<PiggyBank> piggyBanks);
  public Long amountMoney(List<PiggyBank> piggyBanks);  
}
