package com.ccanabalr.piggybank.infraestructure.service;

import com.ccanabalr.piggybank.domain.PiggyBank;

public interface PiggyBankService {

  public PiggyBank save(PiggyBank piggyBank);

  public Integer amountCoins();
  
  public Long amountMoney();

  public Integer amountCoinsByType(Long coin);
  
  public Long amountMoneyByType(Long coin);

}
