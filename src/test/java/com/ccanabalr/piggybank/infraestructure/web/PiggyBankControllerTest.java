package com.ccanabalr.piggybank.infraestructure.web;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import com.ccanabalr.piggybank.application.mapper.PiggyBankMapper;
import com.ccanabalr.piggybank.application.validation.PiggyBankValidation;
import com.ccanabalr.piggybank.domain.PiggyBank;
import com.ccanabalr.piggybank.dto.PiggyBankDto;
import com.ccanabalr.piggybank.infraestructure.service.PiggyBankService;
import com.ccanabalr.piggybank.infraestructure.util.Constants;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PiggyBankControllerTest {

  @Spy
  private PiggyBankService piggyBankService;
  @Spy
  private PiggyBankMapper piggyBankMapper;
  @Spy
  private PiggyBankValidation piggyBankValidation;

  @InjectMocks
  private PiggyBankController piggyBankController;


  String request;
  ResponseEntity<Map<String, Object>> responseSave;

  PiggyBankDto piggyBankDto;
  PiggyBank piggyBank;
  List<PiggyBank> piggyBanks;
  @Before
  public void setUp() throws Exception {
    piggyBankDto = PiggyBankDto.builder().coin("100").build();
    piggyBank = PiggyBank.builder().id(1L).coin(100L).build();
    
   
    piggyBanks = new ArrayList<>();
    piggyBanks.add(piggyBank);
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void saveCreatedTest() throws Exception {
    Map<String, Object> response = new HashMap<>();
    response.put(Constants.MESSAGE, Constants.SUCCESS_PIGGY_BANK);
    piggyBankDto.setId(1L);
    response.put(Constants.PIGGY_BANK, piggyBankDto);
    responseSave = new ResponseEntity<>(response, HttpStatus.CREATED);
    when(piggyBankValidation.piggyBankValidation(piggyBankDto.getCoin())).thenReturn(true);
    when(piggyBankMapper.convertToPiggyBank(piggyBankDto)).thenReturn(piggyBank);
    when(piggyBankService.save(piggyBank)).thenReturn(piggyBank);
    when(piggyBankMapper.convertToPiggyBankDto(piggyBank)).thenReturn(piggyBankDto);
    ResponseEntity<Map<String, Object>> actual = piggyBankController.save(piggyBankDto);
    assertEquals(responseSave, actual);
  }
  
  @Test
  public void saveBadRequestTest() throws Exception {
    Map<String, Object> response = new HashMap<>();
    response.put(Constants.ERROR,
        Constants.VALIDATION_ERROR);
    responseSave = new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    when(piggyBankValidation.piggyBankValidation("101")).thenReturn(false);
    piggyBankDto.setCoin("101");
    ResponseEntity<Map<String, Object>> actual = piggyBankController.save(piggyBankDto);
    assertEquals(responseSave, actual);
  }
  
  @Test
  public void saveInternalErrorTest() throws Exception {
    Map<String, Object> response = new HashMap<>();
    response.put(Constants.ERROR, Constants.ERROR_INSERT);
    responseSave = new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    when(piggyBankValidation.piggyBankValidation(piggyBankDto.getCoin())).thenReturn(true);
    doThrow(new DataAccessException("") {}).when(piggyBankService).save(null);
    ResponseEntity<Map<String, Object>> actual = piggyBankController.save(piggyBankDto);
    assertEquals(responseSave, actual);
  }
  
  @Test 
  public void amountCoinsTest() {
    Map<String, Object> response = new HashMap<>();
    response.put(Constants.MESSAGE,Constants.AMOUNT_COINS_TEXT);
    response.put(Constants.AMOUNT_COINS, 1);
    
    when(piggyBankService.amountCoins()).thenReturn(1);
    ResponseEntity<Map<String, Object>> actual = piggyBankController.amountCoins();
    assertEquals(new ResponseEntity<>(response, HttpStatus.OK) , actual);
  }
  
  @Test 
  public void amountMoneyTest() {
    Map<String, Object> response = new HashMap<>();
    response.put(Constants.MESSAGE, Constants.AMOUNT_MONEY_TEXT);
    response.put(Constants.AMOUNT_MONEY, 100L);
    
    when(piggyBankService.amountMoney()).thenReturn(100L);
    ResponseEntity<Map<String, Object>> actual = piggyBankController.amountMoney();
    assertEquals(new ResponseEntity<>(response, HttpStatus.OK) , actual);
  }
  
  @Test 
  public void amountCoinsByTypeTest() {
    Map<String, Object> response = new HashMap<>();
    response.put(Constants.MESSAGE,Constants.AMOUNT_COINS_BY_TYPE_TEXT);
    response.put(Constants.AMOUNT_COINS_BY_TYPE, 1);
    
    when(piggyBankService.amountCoinsByType(50L)).thenReturn(1);
    ResponseEntity<Map<String, Object>> actual = piggyBankController.amountCoinsByType(50L);
    assertEquals(new ResponseEntity<>(response, HttpStatus.OK) , actual);
  }
  
  @Test 
  public void amountMoneyByTypeTest() {
    Map<String, Object> response = new HashMap<>();
    response.put(Constants.MESSAGE, Constants.AMOUNT_MONEY_BY_TYPE_TEXT);
    response.put(Constants.AMOUNT_MONEY_BY_TYPE, 100L);
    
    when(piggyBankService.amountMoneyByType(100L)).thenReturn(100L);
    ResponseEntity<Map<String, Object>> actual = piggyBankController.amountMoneyByType(100L);
    assertEquals(new ResponseEntity<>(response, HttpStatus.OK) , actual);
  }

}
