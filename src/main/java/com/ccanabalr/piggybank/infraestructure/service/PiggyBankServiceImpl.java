package com.ccanabalr.piggybank.infraestructure.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ccanabalr.piggybank.application.helper.PiggyBankHelper;
import com.ccanabalr.piggybank.domain.PiggyBank;
import com.ccanabalr.piggybank.infraestructure.persistence.PiggyBankRepository;

@Service
public class PiggyBankServiceImpl implements PiggyBankService {
  
  private final PiggyBankRepository piggyBankRepository;
  private final PiggyBankHelper piggyBankHelper;
  
  @Autowired
  public PiggyBankServiceImpl(PiggyBankRepository piggyBankRepository, PiggyBankHelper piggyBankHelper) {
    this.piggyBankRepository = piggyBankRepository;
    this.piggyBankHelper = piggyBankHelper;
  }

  @Override 
  public PiggyBank save(PiggyBank piggyBank) {
    return piggyBankRepository.save(piggyBank);
  }

  @Override
  public Integer amountCoins() {
    return piggyBankHelper.amountCoins(piggyBankRepository.findAll());
  }
  
  @Override
  public Long amountMoney() {
    return piggyBankHelper.amountMoney(piggyBankRepository.findAll());
  }

  @Override
  public Integer amountCoinsByType(Long coin) {
    return piggyBankHelper.amountCoins(piggyBankRepository.findByCoin(coin));
  }

  @Override
  public Long amountMoneyByType(Long coin) {
    return piggyBankHelper.amountMoney(piggyBankRepository.findByCoin(coin));
  }

}
