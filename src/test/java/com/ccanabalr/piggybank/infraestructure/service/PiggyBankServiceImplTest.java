package com.ccanabalr.piggybank.infraestructure.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.ccanabalr.piggybank.application.helper.PiggyBankHelper;
import com.ccanabalr.piggybank.domain.PiggyBank;
import com.ccanabalr.piggybank.dto.PiggyBankDto;
import com.ccanabalr.piggybank.infraestructure.persistence.PiggyBankRepository;


public class PiggyBankServiceImplTest {
  
  @Mock
  private PiggyBankRepository piggyBankRepository;
  @Mock
  private PiggyBankHelper piggyBankHelper;

  @InjectMocks
  private PiggyBankServiceImpl piggyBankServiceImpl;
  
  
  PiggyBankDto piggyBankDto;
  PiggyBank piggyBank;
  List<PiggyBankDto> piggyBankDtos;
  List<PiggyBank> piggyBanks;
  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    piggyBank = PiggyBank.builder().coin(50L).build();
    
    piggyBanks = new ArrayList<>();
    piggyBanks.add(piggyBank);
    
  }
  
  @Test
  public void saveTest() {
    PiggyBank expected = piggyBank;
    expected.setId(1L);
    when(piggyBankRepository.save(piggyBank)).thenReturn(expected);
    PiggyBank actual = piggyBankServiceImpl.save(piggyBank);
    assertNotNull(actual);
    assertEquals(expected, actual);
  }
  
  @Test
  public void amountCoinsTest() {
    when(piggyBankRepository.findAll()).thenReturn(piggyBanks);
    when(piggyBankHelper.amountCoins(piggyBanks)).thenReturn(1);
    Integer actual = piggyBankServiceImpl.amountCoins();
    assertEquals(1, actual);
  }
  
  @Test
  public void amountMoneyTest() {
    when(piggyBankRepository.findAll()).thenReturn(piggyBanks);
    when(piggyBankHelper.amountMoney(piggyBanks)).thenReturn(50L);
    Long actual = piggyBankServiceImpl.amountMoney();
    assertEquals(50L, actual);
  }

  @Test
  public void amountCoinsByTypeTest() {
    when(piggyBankRepository.findByCoin(50L)).thenReturn(piggyBanks);
    when(piggyBankHelper.amountCoins(piggyBanks)).thenReturn(1);
    Integer actual = piggyBankServiceImpl.amountCoinsByType(50L);
    assertEquals(1, actual);
  }
  
  @Test
  public void amountMoneyByTypeTest() {
    when(piggyBankRepository.findByCoin(50L)).thenReturn(piggyBanks);
    when(piggyBankHelper.amountMoney(piggyBanks)).thenReturn(50L);
    Long actual = piggyBankServiceImpl.amountMoneyByType(50L);
    assertEquals(50L, actual);
  }
}
