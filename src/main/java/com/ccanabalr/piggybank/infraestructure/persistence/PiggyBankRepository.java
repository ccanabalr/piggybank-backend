package com.ccanabalr.piggybank.infraestructure.persistence;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.ccanabalr.piggybank.domain.PiggyBank;

@Repository
public interface PiggyBankRepository extends CrudRepository<PiggyBank, Long> {
  
  public List<PiggyBank> findByCoin(Long coin);
  
  public List<PiggyBank> findAll();
 
}
