package com.ccanabalr.piggybank.infraestructure.util;

public final class Constants {
  
  private Constants() {
  }
  
  public static final String MESSAGE = "message";
  public static final String AMOUNT_COINS = "amountCoins";
  public static final String AMOUNT_MONEY = "amountMoney";
  public static final String AMOUNT_COINS_BY_TYPE = "amountCoinsByType";
  public static final String AMOUNT_MONEY_BY_TYPE = "amountMoneyByType";
  public static final String ERROR = "error";
  public static final String PIGGY_BANK = "piggyBank";
  public static final String VALIDATION_ERROR = "Valor incorrecto por favor verificar que su moneda sea de (50,100,200,500,1000)";
  public static final String SUCCESS_PIGGY_BANK = "Se agrego una nueva moneda en la alcancia";
  public static final String ERROR_INSERT = "Error al insertar en la base de datos!";
  public static final String AMOUNT_COINS_TEXT = "Cantidad de monedas en su alcancia";
  public static final String AMOUNT_MONEY_TEXT = "Cantidad de dinero en su alcancia";
  public static final String AMOUNT_COINS_BY_TYPE_TEXT = "Cantidad de monedas por denominación";
  public static final String AMOUNT_MONEY_BY_TYPE_TEXT = "Cantidad de dinero por denominación";

}
