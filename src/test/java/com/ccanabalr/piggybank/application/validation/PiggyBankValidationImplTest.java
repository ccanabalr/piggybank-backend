package com.ccanabalr.piggybank.application.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PiggyBankValidationImplTest {
  
  private PiggyBankValidationImpl piggyBankValidationImpl = new PiggyBankValidationImpl();

  @Before
  public void setUp() throws Exception {}
  
  @Test
  public void piggyBankValidationTrueTest() {
    boolean validation = piggyBankValidationImpl.piggyBankValidation("100");
    assertTrue(validation);
  }
  
  @Test
  public void piggyBankValidationFalseTest() {
    boolean validation = piggyBankValidationImpl.piggyBankValidation("101");
    assertFalse(validation);
  }

}
