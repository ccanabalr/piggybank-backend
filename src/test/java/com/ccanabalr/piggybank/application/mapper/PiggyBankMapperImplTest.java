package com.ccanabalr.piggybank.application.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.ccanabalr.piggybank.domain.PiggyBank;
import com.ccanabalr.piggybank.dto.PiggyBankDto;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PiggyBankMapperImplTest {

  
  PiggyBankMapperImpl piggyBankMapperImpl = new PiggyBankMapperImpl();

  PiggyBankDto piggyBankDto;
  PiggyBank piggyBank;
  List<PiggyBankDto> piggyBankDtos;
  List<PiggyBank> piggyBanks;
  @Before
  public void setUp() throws Exception {
    piggyBankDto = PiggyBankDto.builder().id(1L).coin("50").build();
    piggyBank = PiggyBank.builder().id(1L).coin(50L).build();
    
    piggyBankDtos = new ArrayList<>();
    piggyBankDtos.add(piggyBankDto);
    
    piggyBanks = new ArrayList<>();
    piggyBanks.add(piggyBank);
    
  }
  
  @Test
  public void convertToPiggyBankTest() {
    PiggyBank actual = piggyBankMapperImpl.convertToPiggyBank(piggyBankDto);
    assertEquals(piggyBank.getCoin(), actual.getCoin());
  }
  
  @Test
  public void convertToPiggyBankDtoTest() {
    PiggyBankDto actual = piggyBankMapperImpl.convertToPiggyBankDto(piggyBank);
    assertEquals(piggyBankDto.getCoin(), actual.getCoin());
  }
  
  @Test
  public void convertToPiggyBanksTest() {
    List<PiggyBank> actual = piggyBankMapperImpl.convertToPiggyBanks(piggyBankDtos);
    assertEquals(piggyBanks.size(), actual.size());
  }
  
  @Test
  public void convertToPiggyBankDtosTest() {
    List<PiggyBankDto> actual = piggyBankMapperImpl.convertToPiggyBankDtos(piggyBanks);
    assertEquals(piggyBankDtos.size(), actual.size());
  }

}
