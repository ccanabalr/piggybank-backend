package com.ccanabalr.piggybank.application.validation;

import java.util.regex.Pattern;
import org.springframework.stereotype.Component;

@Component
public class PiggyBankValidationImpl implements PiggyBankValidation {
  
  private static final String COIN_VALIDATION = "^(50|100|200|500|1000)$";

  @Override
  public boolean piggyBankValidation(String coin) {
    return Pattern.matches(COIN_VALIDATION, coin);
  }

}
