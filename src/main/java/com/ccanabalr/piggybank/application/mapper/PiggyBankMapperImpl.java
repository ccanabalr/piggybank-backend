package com.ccanabalr.piggybank.application.mapper;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;
import com.ccanabalr.piggybank.domain.PiggyBank;
import com.ccanabalr.piggybank.dto.PiggyBankDto;

@Component
public class PiggyBankMapperImpl implements PiggyBankMapper {

  @Override
  public PiggyBank convertToPiggyBank(PiggyBankDto piggyBankDto) {
    return PiggyBank.builder().id(piggyBankDto.getId()).coin(Long.valueOf(piggyBankDto.getCoin())).build();
  }

  @Override
  public PiggyBankDto convertToPiggyBankDto(PiggyBank piggyBank) {
    return PiggyBankDto.builder().id(piggyBank.getId()).coin(piggyBank.getCoin().toString()).build();
  }

  @Override
  public List<PiggyBank> convertToPiggyBanks(List<PiggyBankDto> piggyBankDtos) {
    return piggyBankDtos.stream().map(this::convertToPiggyBank).collect(Collectors.toList());
  }

  @Override
  public List<PiggyBankDto> convertToPiggyBankDtos(List<PiggyBank> piggyBanks) {
    return piggyBanks.stream().map(this::convertToPiggyBankDto).collect(Collectors.toList());
  }

}
