package com.ccanabalr.piggybank.application.validation;

public interface PiggyBankValidation {
  
  public boolean piggyBankValidation(String coin);

}
