package com.ccanabalr.piggybank.application.mapper;

import java.util.List;
import com.ccanabalr.piggybank.domain.PiggyBank;
import com.ccanabalr.piggybank.dto.PiggyBankDto;

public interface PiggyBankMapper {
  
  public PiggyBank convertToPiggyBank(PiggyBankDto piggyBankDto);
  
  public PiggyBankDto convertToPiggyBankDto(PiggyBank piggyBank);
  
  public List<PiggyBank> convertToPiggyBanks(List<PiggyBankDto> piggyBankDtos);
  
  public List<PiggyBankDto> convertToPiggyBankDtos(List<PiggyBank> piggyBanks);

}
