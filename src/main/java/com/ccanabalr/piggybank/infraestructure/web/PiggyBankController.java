package com.ccanabalr.piggybank.infraestructure.web;

import java.util.HashMap;
import java.util.Map;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ccanabalr.piggybank.application.mapper.PiggyBankMapper;
import com.ccanabalr.piggybank.application.validation.PiggyBankValidation;
import com.ccanabalr.piggybank.domain.PiggyBank;
import com.ccanabalr.piggybank.dto.PiggyBankDto;
import com.ccanabalr.piggybank.infraestructure.service.PiggyBankService;
import com.ccanabalr.piggybank.infraestructure.util.Constants;

@RestController
@CrossOrigin("*")
@RequestMapping("/piggybank")
public class PiggyBankController {

  private final PiggyBankService piggyBankService;
  private final PiggyBankMapper piggyBankMapper;
  private final PiggyBankValidation piggyBankValidation;

  public PiggyBankController(PiggyBankService piggyBankService, PiggyBankMapper piggyBankMapper,
      PiggyBankValidation piggyBankValidation) {
    this.piggyBankService = piggyBankService;
    this.piggyBankMapper = piggyBankMapper;
    this.piggyBankValidation = piggyBankValidation;
  }

  @PostMapping("/save")
  public ResponseEntity<Map<String, Object>> save(@RequestBody PiggyBankDto piggyBankDto) {
    Map<String, Object> response = new HashMap<>();


      boolean validation = piggyBankValidation.piggyBankValidation(piggyBankDto.getCoin());

      if (!validation) {
        response.put(Constants.ERROR,
            Constants.VALIDATION_ERROR);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
      }

      try {
        
        PiggyBank piggyBank = piggyBankMapper.convertToPiggyBank(piggyBankDto);
        PiggyBank piggyBankNew = piggyBankService.save(piggyBank);
        response.put(Constants.MESSAGE, Constants.SUCCESS_PIGGY_BANK);
        response.put(Constants.PIGGY_BANK, piggyBankMapper.convertToPiggyBankDto(piggyBankNew));
        
      } catch (DataAccessException e) {
        response.put(Constants.ERROR, Constants.ERROR_INSERT);
   
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        
      }
      
      
      return new ResponseEntity<>(response, HttpStatus.CREATED);
  }

  @GetMapping("/amountcoins")
  public ResponseEntity<Map<String, Object>> amountCoins() {
    Map<String, Object> response = new HashMap<>();
    response.put(Constants.MESSAGE,Constants.AMOUNT_COINS_TEXT);
    response.put(Constants.AMOUNT_COINS, piggyBankService.amountCoins());
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  @GetMapping("/amountmoney")
  public ResponseEntity<Map<String, Object>> amountMoney() {
    Map<String, Object> response = new HashMap<>();
    response.put(Constants.MESSAGE, Constants.AMOUNT_MONEY_TEXT);
    response.put(Constants.AMOUNT_MONEY, piggyBankService.amountMoney());
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  @GetMapping("/amountcoinsbytype/{coinType}")
  public ResponseEntity<Map<String, Object>> amountCoinsByType(@PathVariable Long coinType) {
    Map<String, Object> response = new HashMap<>();
    response.put(Constants.MESSAGE, Constants.AMOUNT_COINS_BY_TYPE_TEXT);
    response.put(Constants.AMOUNT_COINS_BY_TYPE, piggyBankService.amountCoinsByType(coinType));
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  @GetMapping("/amountmoneybytype/{coinType}")
  public ResponseEntity<Map<String, Object>> amountMoneyByType(@PathVariable Long coinType) {
    Map<String, Object> response = new HashMap<>();
    response.put(Constants.MESSAGE, Constants.AMOUNT_MONEY_BY_TYPE_TEXT);
    response.put(Constants.AMOUNT_MONEY_BY_TYPE, piggyBankService.amountMoneyByType(coinType));
    return new ResponseEntity<>(response, HttpStatus.OK);
  }


}
