package com.ccanabalr.piggybank.application.helper;

import java.util.List;
import org.springframework.stereotype.Component;
import com.ccanabalr.piggybank.domain.PiggyBank;

@Component
public class PiggyBankHelperImpl implements PiggyBankHelper {

  @Override
  public Integer amountCoins(List<PiggyBank> piggyBanks) {
    return piggyBanks.size();
  }

  @Override
  public Long amountMoney(List<PiggyBank> piggyBanks) {
    return piggyBanks.stream()
        .mapToLong(PiggyBank::getCoin)
        .sum();
  }
  
}
