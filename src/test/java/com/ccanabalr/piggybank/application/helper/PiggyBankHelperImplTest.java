package com.ccanabalr.piggybank.application.helper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.ccanabalr.piggybank.domain.PiggyBank;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PiggyBankHelperImplTest {
  
  PiggyBankHelperImpl piggyBankHelperImpl = new PiggyBankHelperImpl();


  List<PiggyBank> piggyBanks;
  @Before
  public void setUp() throws Exception {
    PiggyBank piggyBank1 = PiggyBank.builder().id(1L).coin(50L).build();
    PiggyBank piggyBank2 = PiggyBank.builder().id(1L).coin(50L).build();
    
    piggyBanks = new ArrayList<>();
    piggyBanks.add(piggyBank1);
    piggyBanks.add(piggyBank2);
    
  }
  
  @Test
  public void amountCoinsTest() {
    Integer amountCoins = piggyBankHelperImpl.amountCoins(piggyBanks);
    assertEquals(2, amountCoins);
  }
  
  @Test
  public void amountMoneyTest() {
    Long amountMoney = piggyBankHelperImpl.amountMoney(piggyBanks);
    assertEquals(100L, amountMoney);
  }

}
